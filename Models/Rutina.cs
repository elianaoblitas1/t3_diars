﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace N00038949_T3.Models
{
    public class Rutina
    {
        public int Id { get; set; }
        public int Id_Usuario { get; set; }
        public string Nombre { get; set; }
        public string Tipo { get; set; }

        public List<Detalle> Detalles { get; set; }
    }
}

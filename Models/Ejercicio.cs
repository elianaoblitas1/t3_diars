﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace N00038949_T3.Models
{
    public class Ejercicio
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Link { get; set; }

        public Detalle Detalle { get; set; }
    }
}

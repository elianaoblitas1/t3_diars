﻿using Microsoft.EntityFrameworkCore;
using N00038949_T3.Models.DB.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace N00038949_T3.Models.DB
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Ejercicio> Ejercicios { get; set; }
        public DbSet<Rutina> Rutinas { get; set; }
        public DbSet<Detalle> Detalles { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new UsuarioConfigurations());
            modelBuilder.ApplyConfiguration(new EjercicioConfigurations());
            modelBuilder.ApplyConfiguration(new RutinaConfigurations());
            modelBuilder.ApplyConfiguration(new DetalleConfigurations());

        }
    }
}

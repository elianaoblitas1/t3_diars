﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace N00038949_T3.Models.DB.Configurations
{
    public class EjercicioConfigurations : IEntityTypeConfiguration<Ejercicio>
    {
        public void Configure(EntityTypeBuilder<Ejercicio> builder)
        {
            builder.ToTable("Ejercicio");
            builder.HasKey(o => o.Id);
            builder.HasOne(o => o.Detalle).WithOne(o => o.Ejercicio).HasForeignKey<Ejercicio>(o => o.Id);

        }
    }
}

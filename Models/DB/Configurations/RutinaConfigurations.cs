﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace N00038949_T3.Models.DB.Configurations
{
    public class RutinaConfigurations : IEntityTypeConfiguration<Rutina>
    {
        public void Configure(EntityTypeBuilder<Rutina> builder)
        {
            builder.ToTable("Rutina");
            builder.HasKey(o => o.Id);
            builder.HasMany(o => o.Detalles).WithOne().HasForeignKey(o => o.Id_Rutina);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace N00038949_T3.Models
{
    public class Detalle
    {
        public int Id { get; set; }
        public int Id_Rutina { get; set; }
        public int Id_Ejercicio { get; set; }
        public int Tiempo { get; set; }

        public Ejercicio Ejercicio { get; set; }


    }
}

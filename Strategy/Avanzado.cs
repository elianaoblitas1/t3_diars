﻿using N00038949_T3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace N00038949_T3.Strategy
{
    public class Avanzado : IStrategy
    {
        public List<Detalle> SeleccionarEjercicios(int rutinaId, List<Ejercicio> ejercicios)
        {
            var random = new Random();
            List<Detalle> ejerciciosRutina = new List<Detalle>();
            for (int i = 0; i < 15; i++)
            {
                var detalle = new Detalle();
                detalle.Id_Rutina = rutinaId;
                int index = random.Next(ejercicios.Count);
                var ejercicio = ejercicios.ElementAt<Ejercicio>(index);
                detalle.Id_Ejercicio = ejercicio.Id;
                detalle.Tiempo = 120;
                ejerciciosRutina.Add(detalle);
            }
            return ejerciciosRutina;
        }
    }
}

﻿using N00038949_T3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace N00038949_T3.Strategy
{
    public class StrategyContext
    {
        private IStrategy strategy;
        public StrategyContext() { }
        public StrategyContext(IStrategy _strategy){
            this.strategy = _strategy;
        }
        public void SetStrategy(IStrategy _strategy)
        {
            this.strategy = _strategy;
        }
        public List<Detalle> Seleccionar_Ejercicios(int id_rutina, List<Ejercicio> ejercicios){
            return this.strategy.SeleccionarEjercicios(id_rutina, ejercicios);
        }
    }
}

﻿using N00038949_T3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace N00038949_T3.Strategy
{
    public interface IStrategy 
    {
        public List<Detalle> SeleccionarEjercicios(int rutinaId, List<Ejercicio> ejercicios);
    }
}

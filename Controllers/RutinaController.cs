﻿using Microsoft.AspNetCore.Mvc;
using N00038949_T3.Models;
using N00038949_T3.Models.DB;
using N00038949_T3.Models.Extensions;
using N00038949_T3.Strategy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace N00038949_T3.Controllers
{
    public class RutinaController : Controller
    {
        private AppDbContext context;
        public RutinaController(AppDbContext _context) {
            this.context = _context;
        }

        public IActionResult Index(int id_rutina)
        {
            var rutina = context.Rutinas.FirstOrDefault(o => o.Id == id_rutina);
            var detalles = context.Detalles.Where(o => o.Id_Rutina == id_rutina);
            List<Ejercicio> ejercicios = new List<Ejercicio>();
            foreach (var item in detalles)
            {
                var ejercicio = context.Ejercicios.FirstOrDefault(o => o.Id == item.Id_Ejercicio);
                ejercicios.Add(ejercicio);
            }
            ViewBag.Detalles = detalles;
            ViewBag.Ejercicios = ejercicios;
            return View(rutina);
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View(new Rutina());
        }
        [HttpPost]
        public IActionResult Create(Rutina rutina)
        {
            if (!ModelState.IsValid)
            {
                return View(rutina);
            }
            var usuario = HttpContext.Session.Get<Usuario>("LoggedUser");
            rutina.Id_Usuario = usuario.Id;
            context.Rutinas.Add(rutina);
            context.SaveChanges();
            var strategyContext = new StrategyContext();
            var ejercicios = context.Ejercicios.ToList();
            switch (rutina.Tipo)
            {
                case "Principiante":
                    strategyContext.SetStrategy(new Principiante());
                    break;
                case "Intermedio":
                    strategyContext.SetStrategy(new Intermedio());
                    break;
                case "Avanzado":
                    strategyContext.SetStrategy(new Avanzado());
                    break;
            }
            var ejerciciosRutina = strategyContext.Seleccionar_Ejercicios(rutina.Id, ejercicios);
            context.Detalles.AddRange(ejerciciosRutina);
            context.SaveChanges();
            return RedirectToAction("Index", "Auth");
        }
    }
}

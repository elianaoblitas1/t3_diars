﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using N00038949_T3.Models;
using N00038949_T3.Models.DB;
using N00038949_T3.Models.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace N00038949_T3.Controllers
{
    public class AuthController : Controller
    {
        private AppDbContext context;
        public AuthController(AppDbContext _context)
        {
            context = _context;
        }

        public IActionResult Index()
        {
            var usuario = HttpContext.Session.Get<Usuario>("LoggedUser");
            var rutinas = context.Rutinas.Where(o => o.Id_Usuario == usuario.Id).ToList();
            return View(rutinas);
        }
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(string nombre, string pass)
        {
            var usuario = context.Usuarios.FirstOrDefault(o => o.Nombre == nombre && o.Password == pass);
            if (usuario == null)
            {
                return View();
            }
            HttpContext.Session.Set("LoggedUser", usuario);
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name,usuario.Nombre),
            };
            var userIdentity = new ClaimsIdentity(claims, "login");
            var principal = new ClaimsPrincipal(userIdentity);
            HttpContext.SignInAsync(principal);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult Registro()
        {
            return View(new Usuario());
        }
        [HttpPost]
        public IActionResult Registro(Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                context.Usuarios.Add(usuario);
                context.SaveChanges();
                return RedirectToAction("Login");
            }
            
            return View(usuario);
        }


    }
}
